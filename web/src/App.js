import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import logo from "./logo.svg";
import "./App.css";

import ListCompany from "./companies/ListCompany";
import ListEmployee from "./employees/ListEmployee";

class App extends Component {
  showCompanies = e => {
    e.preventDefault();

    ReactDOM.render(<ListCompany />, document.getElementById("root"));
  };

  showEmployees = e => {
    e.preventDefault();

    ReactDOM.render(<ListEmployee />, document.getElementById("root"));
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p className="options-main">
            <Link to="/companies">Empresas</Link>
            <Link to="/employees">Empleados</Link>
          </p>
        </header>
      </div>
    );
  }
}

export default App;
