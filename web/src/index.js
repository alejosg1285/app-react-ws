import React from "react";
import ReactDOM from "react-dom";

import { Route, Link, BrowserRouter as Router, Switch } from "react-router-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

import ListCompany from "./companies/ListCompany";
import NewCompany from "./companies/NewCompany";
import EditCompany from "./companies/EditCompany";
import DeleteCompany from "./companies/DeleteCompany";
import ListEmployee from "./employees/ListEmployee";
import NewEmployee from "./employees/NewEmployee";
import EditEmployee from "./employees/EditEmployee";
import DeleteEmployee from "./employees/DeleteEmployee";
import NotFound from "./notfound";

const routing = (
  <Router>
    <Switch>
      <Route exact path="/" component={App} />
      <Route path="/companies" component={ListCompany} />
      <Route path="/new-company" component={NewCompany} />
      <Route path="/edit-company" component={EditCompany} />
      <Route path="/delete-company/:company_id/:name" component={DeleteCompany} />
      <Route path="/employees/:company?" component={ListEmployee} />
      <Route path="/new-employee/:company" component={NewEmployee} />
      <Route path="/edit-employee" component={EditEmployee} />
      <Route path="/delete-employee/:employee" component={DeleteEmployee} />
      <Route component={NotFound} />
    </Switch>
  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
