const validate = (value, rules) => {
    let isValid = true;

    for (let rule in rules) {
        switch(rule) {
            case 'maxLenght':
                isValid = isValid && maxLengthValidator(value, rules[rule]);
                break;
            case 'isRequired':
                isValid = isValid && requiredValidator(value);
                break;
            default:
                isValid = true;
                break;
        }
    }

    return isValid;
}

const maxLengthValidator = (value, maxLength) => {
    return value.length <= maxLength;
};

const requiredValidator = value => value.trim() !== '';

export default validate;