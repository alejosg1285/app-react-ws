import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import { Redirect } from 'react-router-dom';
import ReactDatatable from '@ashvin27/react-datatable';

import NewEmployee from './NewEmployee';
import EditEmployee from './EditEmployee';
import DeleteEmployee from './DeleteEmployee';

export default class ListEmployee extends Component {
    constructor(props) {
        super(props);
        
        //console.log('company', this.props.match.params);
        const { company } = this.props.match.params;
        //console.log('company 2', company);
        this.state = { records: [], company_id: company || '', redirect: false, browseTo: '', record: {} };
        this.columns = [
            {
                key: 'type_id',
                text: 'Tipo de documento',
                className: 'type_id',
                sortable: false
            },
            {
                key: 'name',
                text: 'Nombres',
                className: 'name',
                sortable: true
            },
            {
                key: 'last_name',
                text: 'Apellidos',
                className: 'last_name',
                sortable: true
            },
            {
                key: 'phone',
                text: 'Teléfono',
                className: 'phone',
                sortable: false
            },
            {
                key: 'address',
                text: 'Dirección',
                className: 'address',
                sortable: false
            },
            {
                key: 'email',
                text: 'E-mail',
                className: 'email',
                sortable: false
            },
            {
                key: 'position',
                text: 'Cargo',
                className: 'position',
                sortable: false
            },
            {
                key: 'salary',
                text: 'Salario',
                className: 'salary',
                sortable: false
            },
            {
                key: 'status',
                text: 'Estado',
                className: 'status',
                sortable: false
            },
            {
                key: "action",
                text: "Opciones",
                className: "action",
                align: "left",
                sortable: false,
                cell: record => { 
                    return (
                        <Fragment>
                            <button
                                className="btn btn-primary btn-sm"
                                onClick={() => this.editEmployee(record)}
                                style={{marginRight: '5px'}}>
                                <i className="fa fa-edit"></i>
                            </button>
                            <button 
                                className="btn btn-danger btn-sm" 
                                onClick={() => this.deleteEmployee(record)}>
                                <i className="fa fa-trash"></i>
                            </button>
                        </Fragment>
                    );
                }
            }
        ];
        this.config = {
            page_size: 10,
            length_menu:[10, 20, 50],
            sort: { column: "name", order: "asc" }
        };
    }

    async componentDidMount() {
        let uri = 'http://localhost:3080/employee';

        if (this.state.company_id !== '')
            uri += `/company/${this.state.company_id}`;

        console.log(uri);
        let employees = await fetch(uri);
        let response = await employees.json();

        console.log(response.employees);
        this.setState({ 'records': response.employees });
    }

    newEmployee = () => {
        //ReactDOM.render(<NewEmployee />, document.getElementById('root'));
        this.setState({ redirect: true, browseTo: 'new' });
    }

    editEmployee = (record) => {
        /*ReactDOM.render(<EditEmployee _id={record._id}
            type_id={record.type_id}
            name={record.name}
            last_name={record.last_name}
            phone={record.phone}
            address={record.address}
            email={record.email}
            position={record.position}
            salary={record.salary}
            company_id={record.company_id} />, document.getElementById('root'));*/
        this.setState({ redirect: true, browseTo: 'edit', record: record });
    }

    deleteEmployee = (record) => {
        /*ReactDOM.render(<DeleteEmployee _id={record._id}
            name={record.name}
            last_name={record.last_name} />, document.getElementById("root"));*/
            this.setState({ redirect: true, browseTo: 'delete', record: record });
    }

    render() {
        const { redirect, browseTo } = this.state;

        if (redirect && browseTo === 'new') {
            return <Redirect to={`/new-employee/${this.state.company_id}`} />
        } else if (redirect && browseTo === 'edit') {
            const { record } = this.state;
            return <Redirect to={{ pathname: '/edit-employee', state: { from: record }}} />
        } else if (redirect && browseTo === 'delete') {
            const { record } = this.state;
            return <Redirect to={{ pathname: `/delete-employee/${record._id}`, state: { from: record }}} />
        }

        return (
            <div className="container">
                <h3 className="my-5">Empleados</h3>
                <button type="button" className="btn btn-success my-3" onClick={this.newEmployee}>Nuevo</button>
                <ReactDatatable
                    config={this.config}
                    records={this.state.records}
                    columns={this.columns}
                    />
            </div>
        );
    }
}