import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Redirect } from 'react-router-dom';
import ListEmployee from './ListEmployee';

var serialize = require('form-serialize');

export default class NewComponent extends Component {
    constructor(props) {
        super(props);

        //console.log(this.props.match.params);
        const { company } = this.props.match.params;
        this.state = {
            type_id: '',
            name: '',
            last_name: '',
            phone: '',
            address: '',
            email: '',
            position: '',
            salary: '',
            company_id: company
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);

        this.comeBack = this.comeBack.bind(this);
    }

    handleChange = (evt) => {
        evt.preventDefault();

        let attrName = evt.target.name;
        let attrValue = evt.target.value;

        this.setState({ [attrName]: attrValue });
    }

    async handleSubmit(evt) {
        evt.preventDefault();

        let formData = serialize(evt.target);

        let employee = await fetch('http://localhost:3080/employee', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: formData
        });
        let response = await employee.json();
        console.log(response);
    }

    comeBack = () => {
        //ReactDOM.render(<ListEmployee />, document.getElementById('root'));
        this.setState({ redirect: true });
    }

    render() {
        const { redirect } = this.state;
        if (redirect)
            return <Redirect to={`/employees/${this.state.company_id}`} />

        return (
            <div className="container">
                <h3 className="my-5">Nuevo empleado</h3>
                <form onSubmit={this.handleSubmit}>
                    <input type="hidden" name="company_id" id="company_id" value={this.state.company_id} />
                    <div className="form-group row">
                        <label htmlFor="type_id" className="col-sm-2 col-form-label">Tipo de identificación</label>
                        <div className="col-sm-10">
                        <select id="type_id" name="type_id" defaultValue="" className="form-control" value={this.state.company_type} onChange={this.handleChange}>
                                <option>Choose...</option>
                                <option value="CC">Cédula de ciudadania</option>
                                <option value="TI">Tarjeta de identidad</option>
                                <option value="RC">Registro civil</option>
                                <option value="CE">Cédula de extranjeria</option>
                            </select>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="name" className="col-sm-2 col-form-label">Nombres</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="name" name="name" value={this.state.name} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="last_name" className="col-sm-2 col-form-label">Apellidos</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="last_name" name="last_name" value={this.state.last_name} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="phone" className="col-sm-2 col-form-label">Telefono</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="phone" name="phone" value={this.state.phone} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="address" className="col-sm-2 col-form-label">Dirección</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="address" name="address" value={this.state.address} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="email" className="col-sm-2 col-form-label">E-mail</label>
                        <div className="col-sm-10">
                            <input type="email" className="form-control" id="email" name="email" value={this.state.email} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="position" className="col-sm-2 col-form-label">Cargo</label>
                        <div className="col-sm-10">
                            <input type="position" className="form-control" id="position" name="position" value={this.state.position} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="salary" className="col-sm-2 col-form-label">Salario</label>
                        <div className="col-sm-10">
                            <input type="salary" className="form-control" id="salary" name="salary" value={this.state.salary} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12">
                            <button type="button" className="btn btn-outline-secondary float-left" onClick={this.comeBack}>Volver a empleados</button>
                            <button type="submit" className="btn btn-outline-primary float-right">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}