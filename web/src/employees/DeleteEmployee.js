import React from 'react';
import ReactDOM from 'react-dom';
import { Redirect } from 'react-router-dom';
import ListEmployee from './ListEmployee';

export default class DeleteEmployee extends React.Component {
    constructor(props) {
        super(props);

        const { employee } = this.props.match.params;
        const { from } = this.props.location.state;
        this.state = {
            _id: employee,
            name: from.name,
            last_name: from.last_name,
            company_id: from.company_id,
            redirect: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);

        this.comeBack = this.comeBack.bind(this);
    }

    async handleSubmit(evt) {
        evt.preventDefault();

        let employee = await fetch(`http://localhost:3080/employee/${this.state._id}`, {
            method: 'DELETE'
        });
        let response = await employee.json();
        console.log(response);
    }

    comeBack = () => {
        //ReactDOM.render(<ListEmployee />, document.getElementById('root'));
        this.setState({ redirect: true });
    };

    render() {
        const { redirect } = this.state;
        if (redirect)
            return <Redirect to={`/employees/${this.state.company_id}`} />

        return (
            <div className="container">
                <h3 className="my-5">Eliminar empleado</h3>
                <div className="row">
                    <div className="col-md-6 offset-md-3">
                        <form onSubmit={this.handleSubmit}>
                            <input type="hidden" name="id" id="id" value={this.state._id} />
                            <div className="form-group row">
                                <label className="col-sm-12 col-form-label text-center">
                                    {"¿Está seguro de eliminar la empresa " + this.state.name + " "+ this.state.last_name + "?"}
                                </label>
                            </div>
                            <div className="row">
                                <div className="col-sm-12">
                                    <button type="button" className="btn btn-outline-secondary float-left" onClick={this.comeBack}>Volver a empleados</button>
                                    <button type="submit" className="btn btn-outline-danger float-right">Eliminar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}