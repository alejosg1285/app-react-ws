import React from 'react';
import ReactDOM from 'react-dom';
import { Redirect } from 'react-router-dom';
import ListCompany from './ListCompany';

var serialize = require('form-serialize');

export default class EditCompany extends React.Component {
    constructor(props) {
        super(props);
        //console.log(props.location.state);
        const { from } = props.location.state;
        this.state = {
            _id: from._id,
            nit: from.nit,
            name: from.name,
            address: from.address,
            phone: from.phone,
            email: from.email,
            company_type: from.company_type,
            redirect: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);

        this.comeBack = this.comeBack.bind(this);
    }

    handleChange = (e) => {
        e.preventDefault();

        let attrName = e.target.name;
        let attrValue = e.target.value;
        
        this.setState({ [attrName]: attrValue });
    };

    async handleSubmit(evt) {
        evt.preventDefault();

        let formBody = serialize(evt.target);

        let company = await fetch('http://localhost:3080/company/'+this.state._id, { method: 'PUT', headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }, body: formBody });
        let response = await company.json();
        console.log(response);
    }

    comeBack = () => {
        //ReactDOM.render(<ListCompany />, document.getElementById('root'));
        this.setState({ redirect: true });
    }

    render() {
        const { redirect } = this.state;
        if (redirect)
            return <Redirect to="/companies" />

        return (
            <div className="container">
                <h3 className="my-5">Editar empresa</h3>
                <form onSubmit={this.handleSubmit}>
                    <input type="hidden" name="id" id="id" value={this.state._id} />
                    <div className="form-group row">
                        <label htmlFor="nit" className="col-sm-2 col-form-label">NIT</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="nit" name="nit" value={this.state.nit} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="name" className="col-sm-2 col-form-label">Nombre</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="name" name="name" value={this.state.name} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="address" className="col-sm-2 col-form-label">Dirección</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="address" name="address" value={this.state.address} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="phone" className="col-sm-2 col-form-label">Telefono</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="phone" name="phone" value={this.state.phone} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="email" className="col-sm-2 col-form-label">E-mail</label>
                        <div className="col-sm-10">
                            <input type="email" className="form-control" id="email" name="email" value={this.state.email} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="company_type" className="col-sm-2 col-form-label">Tamaño</label>
                        <div className="col-sm-10">
                            <select id="company_type" name="company_type" className="form-control" value={this.state.company_type} onChange={this.handleChange}>
                                <option>Choose...</option>
                                <option value="MICRO">Micro</option>
                                <option value="SMALL">Pequeña</option>
                                <option value="MEDIUM">Mediana</option>
                                <option value="LARGE">Grande</option>
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12">
                            <button type="button" className="btn btn-outline-secondary float-left" onClick={this.comeBack}>Volver a empresas</button>
                            <button type="submit" className="btn btn-outline-primary float-right">Actualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}