import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import ListCompany from "./ListCompany";

var serialize = require("form-serialize");

class NewCompany extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nit: "",
      name: "",
      address: "",
      phone: "",
      email: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.comeBack = this.comeBack.bind(this);
  }

  handleChange = e => {
    e.preventDefault();

    let attrName = e.target.name;
    let attrValue = e.target.value;

    this.setState({ [attrName]: attrValue });
  };

  async handleSubmit(evt) {
    evt.preventDefault();

    let formBody = serialize(evt.target);

    let company = await fetch("http://localhost:3080/company", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: formBody
    });
    let response = await company.json();
    console.log(response);
  }

  comeBack = () => {
    ReactDOM.render(<ListCompany />, document.getElementById("root"));
  };

  render() {
    return (
      <div className="container">
        <h3 className="my-5">Nueva empresa</h3>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group row">
            <label htmlFor="nit" className="col-sm-2 col-form-label">
              NIT
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                className="form-control"
                id="nit"
                name="nit"
                value={this.state.nit}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="name" className="col-sm-2 col-form-label">
              Nombre
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                className="form-control"
                id="name"
                name="name"
                value={this.state.name}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="address" className="col-sm-2 col-form-label">
              Dirección
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                className="form-control"
                id="address"
                name="address"
                value={this.state.address}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="phone" className="col-sm-2 col-form-label">
              Telefono
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                className="form-control"
                id="phone"
                name="phone"
                value={this.state.phone}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="email" className="col-sm-2 col-form-label">
              E-mail
            </label>
            <div className="col-sm-10">
              <input
                type="email"
                className="form-control"
                id="email"
                name="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="company_type" className="col-sm-2 col-form-label">
              Tamaño
            </label>
            <div className="col-sm-10">
              <select
                id="company_type"
                name="company_type"
                defaultValue=""
                className="form-control"
                value={this.state.company_type}
                onChange={this.handleChange}
              >
                <option>Choose...</option>
                <option value="MICRO">Micro</option>
                <option value="SMALL">Pequeña</option>
                <option value="MEDIUM">Mediana</option>
                <option value="LARGE">Grande</option>
              </select>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12">
              <Link
                className="btn btn-outline-secondary float-left"
                to="/companies"
              >
                Volver a empresas
              </Link>
              <button
                type="submit"
                className="btn btn-outline-primary float-right"
              >
                Registrar
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default NewCompany;
