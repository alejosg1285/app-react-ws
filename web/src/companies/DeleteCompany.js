import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Redirect } from 'react-router-dom';
import ListCompany from './ListCompany';

export default class DeleteCompany extends Component {
    constructor(props) {
        super(props);
        
        const { company_id, name } = this.props.match.params;
        this.state = {
            _id: company_id,
            name: name,
            redirect: false
        }

        this.handleSubmit = this.handleSubmit.bind(this);

        this.comeBack = this.comeBack.bind(this);
    }

    async handleSubmit(evt) {
        evt.preventDefault();

        let company = await fetch('http://localhost:3080/company/'+this.state._id, { method: 'DELETE', headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          } });
        let response = await company.json();
        console.log(response);
    }

    comeBack = () => {
        this.setState({ redirect: true });
        //ReactDOM.render(<ListCompany />, document.getElementById('root'));
    }

    render() {
        const { redirect } = this.state;
        if (redirect)
            return <Redirect to="/companies" />;

        return (
            <div className="container">
                <h3 className="my-5">Eliminar empresa</h3>
                <div className="row">
                    <div className="col-md-6 offset-md-3">
                        <form onSubmit={this.handleSubmit}>
                            <input type="hidden" name="id" id="id" value={this.state._id} />
                            <div className="form-group row">
                                <label className="col-sm-12 col-form-label text-center">
                                    {"¿Está seguro de eliminar la empresa " + this.state.name + "?"}
                                </label>
                            </div>
                            <div className="row">
                                <div className="col-sm-12">
                                    <button type="button" className="btn btn-outline-secondary float-left" onClick={this.comeBack}>Volver a empresas</button>
                                    <button type="submit" className="btn btn-outline-danger float-right">Eliminar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}