import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { Link, Redirect } from "react-router-dom";
import ReactDatatable from "@ashvin27/react-datatable";

import NewCompany from "./NewCompany";
import EditCompany from "./EditCompany";
import DeleteCompany from "./DeleteCompany";
import ListEmployee from "../employees/ListEmployee";

class ListCompany extends Component {
  constructor(props) {
    super(props);

    this.state = { records: [], redirect: false, company_id: '', browseTo: '', record: {} };
    this.columns = [
      {
        key: "nit",
        text: "NIT",
        className: "nit",
        sortable: false
      },
      {
        key: "name",
        text: "Empresa",
        className: "name",
        sortable: true
      },
      {
        key: "address",
        text: "Dirección",
        className: "address",
        sortable: false
      },
      {
        key: "phone",
        text: "Teléfono",
        className: "phone",
        sortable: false
      },
      {
        key: "email",
        text: "E-mail",
        className: "email",
        sortable: false
      },
      {
        key: "company_type",
        text: "Tamaño de la empresa",
        className: "company_type",
        sortable: false
      },
      {
        key: "status",
        text: "Estado",
        className: "status",
        sortable: false
      },
      {
        key: "action",
        text: "Opciones",
        className: "action",
        align: "left",
        sortable: false,
        cell: record => {
          return (
            <Fragment>
              <button
                className="btn btn-secondary btn-sm"
                onClick={() => this.showEmployees(record)}
                style={{ marginRight: "5px" }}
              >
                <i className="fa fa-users" />
              </button>
              <button
                className="btn btn-primary btn-sm"
                onClick={() => this.editCompany(record)}
                style={{ marginRight: "5px" }}
              >
                <i className="fa fa-edit" />
              </button>
              <button
                className="btn btn-danger btn-sm"
                onClick={() => this.deleteCompany(record)}
              >
                <i className="fa fa-trash" />
              </button>
            </Fragment>
          );
        }
      }
    ];
    this.config = {
      page_size: 10,
      length_menu: [10, 20, 50],
      sort: { column: "name", order: "asc" }
    };
  }

  async componentDidMount() {
    let companies = await fetch("http://localhost:3080/company");
    let response = await companies.json();

    console.log(response.companies);
    this.setState({ records: response.companies });
  }

  editCompany(record) {
    /*console.log("Edit Record", record);
    return <Redirect to='/edit-company' />
    ReactDOM.render(
      <EditCompany
        _id={record._id}
        nit={record.nit}
        name={record.name}
        address={record.address}
        phone={record.phone}
        email={record.email}
        company_type={record.company_type}
      />,
      document.getElementById("root")
    );*/
    this.setState({ redirect: true, company_id: record._id, browseTo: 'edit', record: record });
  }

  deleteCompany(record) {
    /*console.log("Delete Record", record);
    ReactDOM.render(
      <DeleteCompany _id={record._id} name={record.name} />,
      document.getElementById("root")
    );*/
    this.setState({ redirect: true, company_id: record._id, browseTo: 'delete', record: record });
  }

  showEmployees = record => {
    /*ReactDOM.render(
      <ListEmployee company_id={record._id} />,
      document.getElementById("root")
    );*/
    this.setState({ redirect: true, company_id: record._id, browseTo: 'employees' });
  };

  newCompany = () => {
    //ReactDOM.render(<NewCompany />, document.getElementById("root"));
    this.setState({ redirect: true, browseTo: 'new' });
  };

  render() {
    const { redirect, company_id, browseTo } = this.state;
    if (redirect && browseTo === 'employees')
      return <Redirect to={`/employees/${company_id}`} />;
    else if (redirect && browseTo === 'new')
      return <Redirect to="/new-company" />;
    else if (redirect && browseTo === 'delete') {
      const { record } = this.state;
      return <Redirect to={`/delete-company/${company_id}/${record.name}`} />;
    } else if (redirect && browseTo === 'edit') {
      const { record } = this.state;
      return <Redirect to={{ pathname: '/edit-company', state: { from: record }}} />;
    }

    return (
      <div className="container">
        <h3 className="my-5">Empresas:</h3>
        <Link className="btn btn-success my-3" to="/new-company">
          Nuevo
        </Link>
        <ReactDatatable
          config={this.config}
          records={this.state.records}
          columns={this.columns}
        />
      </div>
    );
  }
}

export default ListCompany;
