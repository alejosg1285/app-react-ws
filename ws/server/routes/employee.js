const express = require('express');
let Employee = require('../models/employee');

let app = express();
app.use(express.urlencoded());

// Get employees list
app.get('/employee', (req, res) => {
    Employee.find()
        .populate('company_id', 'name')
        .exec((err, employees) => {
            if (err) {
                return res.status(500).json({
                    result: false,
                    err
                });
            }

            return res.json({
                result: true,
                employees
            });
        });
});

// Get employee by id.
app.get('/employee/:id', (req, res) => {
    let id = req.params.id;

    Employee.findById(id, (err, employeeDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!employeeDb) {
            return res.status(404).json({
                result: false,
                err: { message: `Employee with id '${id}' not found` }
            });
        }

        return res.json({
            result: true,
            employee: employeeDb
        });
    });
});

// Get Employees by company.
app.get('/employee/company/:id', (req, res) => {
    let id = req.params.id;

    Employee.find({ company_id: id })
    .populate('company_id', 'name')
    .exec((err, employees) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!employees) {
            return res.status(404).json({
                result: false,
                err: { message: `Employees with company id '${id}' not found` }
            });
        }

        return res.json({
            result: true,
            employees
        });
    });
});

// Create a employee
app.post('/employee', (req, res) => {
    let body = req.body;

    let employee = new Employee({
        type_id: body.type_id,
        name: body.name,
        last_name: body.last_name,
        phone: body.phone,
        address: body.address,
        email: body.email,
        position: body.position,
        salary: body.salary,
        company_id: body.company_id
    });

    employee.save((err, employeeDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!employeeDb) {
            return res.status(400).json({
                result: false,
                err
            });
        }

        return res.json({
            result: true,
            employee: employeeDb
        });
    });
});

// Update an employee
app.put('/employee/:id', (req, res) => {
    let id = req.params.id;
    let body = req.body;

    let employee = {
        type_id: body.type_id,
        name: body.name,
        last_name: body.last_name,
        phone: body.phone,
        address: body.address,
        email: body.email,
        position: body.position,
        salary: body.salary,
        company_id: body.company_id
    };

    Employee.findByIdAndUpdate(id, employee, { new: true, runValidators: true }, (err, employeeDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!employeeDb) {
            return res.status(404).json({
                result: false,
                err: { message: `Employee with id '${id}' not found` }
            });
        }

        return res.json({
            result: true,
            employee: employeeDb
        });
    });
});

// Delete an employee by id.
app.delete('/employee/:id', (req, res) => {
    let id = req.params.id;

    let employee = { status: false };

    Employee.findByIdAndUpdate(id, employee, { new: true }, (err, employeeDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!employeeDb) {
            return res.status(404).json({
                result: false,
                err: { message: `Employee with id '${id}' not found` }
            });
        }

        return res.json({
            result: true,
            employee: employeeDb
        });
    });
});

module.exports = app;