const express = require('express');

const app = express();

app.use(require('./company'));
app.use(require('./employee'));

module.exports = app;