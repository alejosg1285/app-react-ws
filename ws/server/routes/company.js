const express = require('express');
const cors = require('cors');
let Company = require('../models/company');

let app = express();
app.use(cors());
app.use(express.urlencoded());

// Get companies list
app.get('/company', function(req, res) {
    Company.find()
        .exec((err, companyDb) => {
            if (err) {
                return res.status(400).json({
                    result: false,
                    err
                });
            }

            return res.json({
                result: true,
                companies: companyDb
            });
        });
});

// Get a company by id
app.get('/company/:id', (req, res) => {
    let id = req.params.id;

    Company.findById(id, (err, companyDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!companyDb) {
            return res.status(404).json({
                result: false,
                err: { message: `Company with id '${id}' not found` }
            });
        }

        return res.json({
            result: true,
            company: companyDb
        });
    });
});

// Create a company.
app.post('/company', (req, res) => {
    let body = req.body;
    
    let company = new Company({
        nit: body.nit,
        name: body.name,
        company_type: body.company_type,
        phone: body.phone,
        address: body.address,
        email: body.email
    });

    company.save((err, companyDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!companyDb) {
            return res.status(400).json({
                result: false,
                err
            });
        }

        return res.json({
            result: true,
            company: companyDb
        });
    });
});

// Update a company.
app.put('/company/:id', (req, res) => {
    let id = req.params.id;
    let body = req.body;

    let company = {
        nit: body.nit,
        name: body.name,
        company_type: body.company_type,
        phone: body.phone,
        address: body.address,
        email: body.email
    };

    Company.findByIdAndUpdate(id, company, { new: true, runValidators: true }, (err, companyDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!companyDb) {
            return res.status(404).json({
                result: false,
                err: { message: `Company with id '${id}' not found` }
            });
        }

        return res.json({
            result: true,
            company: companyDb
        });
    });
});

// Delete a company by id.
app.delete('/company/:id', (req, res) => {
    let id = req.params.id;

    let company = { status: false };

    Company.findByIdAndUpdate(id, company, { new: true }, (err, companyDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!companyDb) {
            return res.status(404).json({
                result: false,
                err: { message: `Company with id '${id}' not found` }
            });
        }

        return res.json({
            result: true,
            company: companyDb
        });
    });
});

module.exports = app;