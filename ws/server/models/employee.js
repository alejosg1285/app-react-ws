const mongoose = require('mongoose');

let Schema = mongoose.Schema;
let typesIdValid = {
    values: ['CC', 'TI', 'RC', 'CE'],
    message: '{VALUE} no es un tipo de identificación valido'
};

let employeeSchema = new Schema({
    type_id: {
        type: String,
        default: 'CC',
        enum: typesIdValid
    },
    name: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    last_name: {
        type: String,
        required: [true, 'Los apellidos son obligatorios']
    },
    phone: {
        type: String,
        required: false
    },
    address: {
        type: String,
        required: [true, 'La dirección es obligatoria']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'El correo electronico es obligatorio']
    },
    position: {
        type: String,
        required: [true, 'El cargo es obligatorio']
    },
    salary: {
        type: Number,
        required: [true, 'El salario es obligatorio']
    },
    company_id: {
        type: Schema.Types.ObjectId,
        ref: 'Company',
        required: [true, 'La empresa es obligatoria']
    },
    status: {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('Employee', employeeSchema);