const mongoose = require('mongoose');

let Schema = mongoose.Schema;
let typesCompanyValid = {
    values: ['MICRO', 'SMALL', 'MEDIUM', 'LARGE'],
    message: '{VALUE} no es un tipo de empresa valido'
};

let companySchema = new Schema({
    nit: {
        type: String,
        required: [true, 'El NIT es obligatorio']
    },
    name: {
        type: String,
        required: [true, 'El nombre de la empresa es obligatorio']
    },
    company_type: {
        type: String,
        default: 'MICRO',
        enum: typesCompanyValid
    },
    phone: {
        type: String,
        required: false
    },
    address: {
        type: String,
        required: [true, 'La dirección es obligatoria']
    },
    email: {
        type: String,
        required: [true, 'El correo electronico es obligatorio']
    },
    status: {
        type: Boolean,
        default: true
    }
});

/*companySchema.methods.toJSON = function() {
    let company = this;
    let companyObject = company.toObject();

    return companyObject;
}*/

module.exports = mongoose.model('Company', companySchema);