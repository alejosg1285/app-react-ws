const express = require('express');
const mongoose = require('mongoose');

require('./config/config');

const app = express();

// Configuracion global de rutas
app.use(require('./routes/index'));

app.get('/', (req, res) => {
    res.send('Hello, lets to communicate with React!');
});

mongoose.connect(URL_DB, { useNewUrlParser: true, useCreateIndex: true }, (err, resp) => {
    if (err) throw err;

    console.log('Conexion establecida');
});

app.listen(PORT, () => {
    console.log(`Server listen on port ${PORT}`);
});